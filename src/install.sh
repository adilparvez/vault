#!/usr/bin/env bash

sudo mkdir /opt/vault
sudo cp vault vault.py LICENSE /opt/vault
echo 'export PATH=$PATH:/opt/vault' >> ~/.bashrc
