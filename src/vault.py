import argparse
import getpass
import hashlib
import json
import os
import re
import subprocess
import sys


# Path to store currently open
VAULT_PATH="/opt/vault"
VAULTS_JSON_PATH=os.path.join(VAULT_PATH, "vaults.json")


def main():
    parser = argparse.ArgumentParser("vault")
    subparsers = parser.add_subparsers(dest="subcommand")

    new_parser = subparsers.add_parser("new", help="create new vault")
    new_parser.add_argument("file", help="file name")
    new_parser.add_argument("size", help="size of vault, e.g. 256MiB")
    new_parser.add_argument("-p", "--password", help="password of vault")
    new_parser.add_argument("-f", "--force", action="store_true", help="force create if file already exists")

    open_parser = subparsers.add_parser("open", help="open vault")
    open_parser.add_argument("file", help="file name")
    open_parser.add_argument("-p", "--password", help="password of vault")

    close_parser = subparsers.add_parser("close", help="close vault")
    close_parser.add_argument("vault", help="path of vault or id with --id")
    close_parser.add_argument("--id", action="store_true", help="id of vault to close")

    list_parser = subparsers.add_parser("list", help="list open vaults")

    close_all_parser = subparsers.add_parser("close-all", help="close all open vaults")

    args = parser.parse_args()

    check_root()

    if args.subcommand == "new":
        match = re.match(r"^([0-9]+)(MiB|GiB)$", args.size)
        if match:
            unit = match.group(2)
            num_mebibytes = int(match.group(1))
            if unit == "GiB":
                num_mebibytes *= 1024
            if num_mebibytes < 3:
                print("Container too small.")
                sys.exit(0)
            vault_new(os.path.abspath(args.file), num_mebibytes, args.password, args.force)
        else:
            print("Size must be of the form <number>MiB or <number>GiB.")
    elif args.subcommand == "open":
        vault_open(os.path.abspath(args.file), args.password)
    elif args.subcommand == "close":
        vault_close(args.vault, args.id)
    elif args.subcommand == "close-all":
        vault_close_all()
    elif args.subcommand == "list":
        vault_list()
    else:
        print(parser.format_help())


# This script needs root for some of its operations.
def check_root():
    if os.geteuid() != 0:
        sys.exit("Run as root.")


# Create a new vault at 'path' of size 'num_mebibytes', 'force' controls overwrite behaviour.
def vault_new(path, num_mebibytes, password, force):
    if os.path.exists(path) and not force:
        print("File already exists.")
        sys.exit(0)

    # Get password from stdin if it wasn't passed in as an argument.
    if not password:
        while True:
            password1 = getpass.getpass("Password: ")
            password2 = getpass.getpass("Verify password: ")
            if password1 == password2:
                password = password1
                break
            else:
                print("Password mismatch, try again.")

    # Create a file with data from /dev/random using dd, also print a progress bar.
    print_flush("Allocating file... ")
    dd = subprocess.Popen(["dd", "if=/dev/urandom",
                                 "of={}".format(path),
                                 "bs=1MiB",
                                 "count={}".format(num_mebibytes),
                                 "status=progress"
                          ],
                          stderr=subprocess.PIPE, # Manpage says status output goes to stderr.
                          universal_newlines=True # Status output with carriage return, need this to read line rewrites.
                         )
    print_flush("\rAllocating file... [{}] 0.00%".format(" "*25))
    for line in dd.stderr:
        match = re.match(r"^([0-9]+) bytes", line)
        if match:
            num_bytes = int(match.group(1))
            num_mebibytes_done = float(num_bytes) / (1024 * 1024)
            proportion = num_mebibytes_done / num_mebibytes
            print_progress_bar("Allocating file... ", proportion, 25)
    dd.terminate()
    print(" Done.")

    subprocess.call(["chown", "{0}:{0}".format(os.getenv("SUDO_UID")), path])

    # Create LUKS container.
    print_flush("Creating LUKS container... ")
    cryptsetup = subprocess.Popen(["cryptsetup", "--batch-mode", "luksFormat", path],
                                  stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE)
    cryptsetup.communicate("{}\n".format(password).encode("utf-8"))
    cryptsetup.terminate()
    print("Done.")

    # Create file system and mount.
    device = map_name(path)

    print_flush("Creating file system... ")
    cryptsetup = subprocess.Popen(["cryptsetup", "luksOpen", path, device],
                                  stdin=subprocess.PIPE)
    cryptsetup.communicate("{}\n".format(password).encode("utf-8"))
    cryptsetup.terminate()

    subprocess.call(["mkfs.ext4", "/dev/mapper/{}".format(device)],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
    print("Done.")

    subprocess.call(["mkdir", "{}-mnt".format(path)],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)

    subprocess.call(["mount", "/dev/mapper/{}".format(device), "{}-mnt".format(path)],
                    stdout=subprocess.DEVNULL)

    subprocess.call(["chown", "{0}:{0}".format(os.getenv("SUDO_UID")), "{}-mnt".format(path)])

    print("Mounted at: {}-mnt".format(path))

    add_vault(path)


# Open the vault at 'path'.
def vault_open(path, password):
    if not os.path.exists(path):
        print("File doesn't exist.")
        sys.exit(0)

    if vault_is_open(path):
        print("Already open. Mounted at: {}-mnt".format(path))
        sys.exit(0)

    device = map_name(path)

    # Three attempts at password entry.
    if not password:
        trys = 0
        while trys < 3:
            password = getpass.getpass("Password: ")
            if password == "":
                trys += 1
                print("Password incorect.")
                continue

            cryptsetup = subprocess.Popen(["cryptsetup", "luksOpen", path, device],
                                          stdin=subprocess.PIPE,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)
            out, err = cryptsetup.communicate("{}\n".format(password).encode("utf-8"))
            cryptsetup.terminate()

            if err.decode("utf-8") == "":
                break
            trys += 1
            print("Password incorect.")

        if trys == 3:
            sys.exit(0)
    else:
        cryptsetup = subprocess.Popen(["cryptsetup", "luksOpen", path, device],
                                      stdin=subprocess.PIPE,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
        out, err = cryptsetup.communicate("{}\n".format(password).encode("utf-8"))
        cryptsetup.terminate()

        if err.decode("utf-8") != "":
            print("Password incorect.")
            sys.exit(0)

    subprocess.call(["mkdir", "{}-mnt".format(path)],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)

    subprocess.call(["mount", "/dev/mapper/{}".format(device), "{}-mnt".format(path)],
                    stdout=subprocess.DEVNULL)

    subprocess.call(["chown", "{0}:{0}".format(os.getenv("SUDO_UID")), "{}-mnt".format(path)])

    print("Mounted at: {}-mnt".format(path))

    add_vault(path)


# Close vault, 'id_flag' determines if we treat 'vault' as an id or a path.
def vault_close(vault, id_flag):
    if id_flag:
        try:
            vault_id = int(vault)
            vault_close_id(vault_id)
        except ValueError:
            print("ID must be a non-negative integer.")
    else:
        path = os.path.abspath(vault)
        vault_close_path(path)


# Close the vault at 'path'.
def vault_close_path(path):
    if not os.path.exists(path):
        print("File doesn't exist.")
        sys.exit(0)

    ret = subprocess.call(["umount", "{}-mnt".format(path)],
                    stderr=subprocess.DEVNULL)

    if ret != 0:
        print("Not open.")
        sys.exit(0)

    subprocess.call(["rm", "-r", "{}-mnt".format(path)],
                    stderr=subprocess.DEVNULL)

    subprocess.call(["cryptsetup", "luksClose", map_name(path)],
                    stderr=subprocess.DEVNULL)

    remove_vault(path)


# Close vault with id 'id'.
def vault_close_id(id):
    vaults = load_json(VAULTS_JSON_PATH).setdefault("vaults", [])
    if id not in range(0, len(vaults)):
        print("ID out of range.")
        sys.exit(0)
    vault_close_path(vaults[id])


# Close all open vaults.
def vault_close_all():
    vaults = load_json(VAULTS_JSON_PATH).setdefault("vaults", [])
    for vault in vaults:
        vault_close(vault, False)

# List all open vaults.
def vault_list():
    vaults = load_json(VAULTS_JSON_PATH).setdefault("vaults", [])
    for idx, vault in enumerate(vaults):
        print("{} -> {}".format(idx, vault))


# Load the json in 'file' into a pyhton dict.
def load_json(file):
    try:
        with open(file) as f:
            return json.load(f)
    except FileNotFoundError:
        return {}


# Save the dict 'obj' as json into 'file'.
def save_json(file, obj):
    with open(file, "w") as f:
        json.dump(obj, f, indent=4)


# Add 'path' into the list of open vaults in vaults.json.
def add_vault(path):
    obj = load_json(VAULTS_JSON_PATH)
    obj.setdefault("vaults", []).append(path)
    save_json(VAULTS_JSON_PATH, obj)


# Remove 'path' from the list of open vaults in vaults.json.
def remove_vault(path):
    obj = load_json(VAULTS_JSON_PATH)
    try:
        obj.setdefault("vaults", []).remove(path)
    except ValueError:
        pass
    save_json(VAULTS_JSON_PATH, obj)


# Check if vault already open by looking in vaults.json.
def vault_is_open(path):
    return path in load_json(VAULTS_JSON_PATH).setdefault("vaults", [])


# Map the path of the vault file to a device name.
def map_name(path):
    return "vault-{}".format(hashlib.sha256(path.encode("utf-8")).hexdigest())


def print_flush(str):
    sys.stdout.write(str)
    sys.stdout.flush()


def print_progress_bar(prefix, proportion, bar_width):
    done_width = int(round(bar_width * proportion))
    text = "\r{0}[{1}] {2:.2f}%".format(prefix, "█"*done_width + " "*(bar_width - done_width), proportion * 100)
    print_flush(text)


if __name__ == "__main__":
    main()
