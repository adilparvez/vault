# vault

A tool for creating and using dmcrypt+LUKS encrypted containers with a very simple UI.

## Why?
It's a common enough operation, but the UX of using dd + cryptsetup + mkfs + mount isn't that great, vault is a python script that ties these together and exposes a simple and pleasant interface.

## Usage
```
# Make a new encrypted container, initial data comes from /dev/urandom.
$ vault new foo 512MiB
Password:
Verify password:
Allocating file... [█████████████████████████] 100.00% Done.
Creating LUKS container... Done.
Creating file system... Done.
Mounted at: /home/x/foo-mnt

# List all open vaults with their ids.
$ vault list
0 -> /home/x/foo

# Close a vault with a particular id.
$ vault close --id 0

# Close a vault at a particular path.
$ vault close foo

# Open a vault.
$ vault open foo
Password:
Mounted at: /home/x/foo-mnt

# Close all open vaults.
$ vault close-all
```

![demo](demo.gif)

## Release(s)
[vault v1.1.0](https://gitlab.com/adilparvez/vault/raw/master/release/1/1/0/vault)

~~[vault v1.0.1](https://gitlab.com/adilparvez/vault/raw/master/release/1/0/1/vault)~~

~~[vault v1.0.0](https://gitlab.com/adilparvez/vault/raw/master/release/1/0/0/vault)~~

## Dependencies
GNU coreutils >= 8.24, I use dd's status flag. This is satisfied by default on Ubuntu >= 16.04.

## Install
```
tar xzvf vault.tar.gz
./install.sh
source ~/.bashrc
```

## Uninstall
```
./uninstall.sh
```

## License

The MIT License (MIT)
Copyright (c) 2016 Adil Parvez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
